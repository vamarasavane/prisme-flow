Install the Heroku CLI

Download and install the Heroku CLI.

If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.

$ heroku login
Create a new Git repository

Initialize a git repository in a new or existing directory

$ cd my-project/
$ git init
$ heroku git:remote -a prisme-flow
Deploy your application

Commit your code to the repository and deploy it to Heroku using Git.

$ git add .
$ git commit -am "make it better"
$ git push heroku master
Existing Git repository

For existing repositories, simply add the heroku remote

$ heroku git:remote -a prisme-flow




git remote add origin https://vamarasavane@bitbucket.org/prismeio/prisme-flow.git
git push -u origin master

git clone https://vamarasavane@bitbucket.org/prismeio/prisme-flow.git
cd hello
echo "# My project's README" >> README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master