prisme-flow-heroku
================

A wrapper for deploying [prisme-flow](http://prisme.io) into the [Heroku](https://www.heroku.com).

### Deploying prisme-flow into Heroku

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy?template=https://vamarasavane@bitbucket.org/prismeio/prisme-flow)
