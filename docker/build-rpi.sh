if [ $# -ne 1 ]; then
    echo $0: usage: build-rpi.sh PRISME_FLOW_VERSION
    exit 1
fi

PRISME_FLOW_VERSION=$1.0.17

docker build -f rpi/Dockerfile -t prisme-flow/prisme-flow-docker:rpi .

if [ $? -ne 0 ]; then
    echo "ERROR: Docker build failed for rpi image"
    exit 1
fi

docker tag prismeflow/prisme-flow-docker:rpi prisme-flow/prisme-flow:$PRISME_FLOW_VERSION-rpi

if [ $? -ne 0 ]; then
    echo "ERROR: Docker tag failed for rpi image"
    exit 1
fi

docker run -d prismeflow/prisme-flow-docker:rpi

if [ $? -ne 0 ]; then
    echo "ERROR: Docker container failed to start for rpi build."
    exit 1
fi

docker push prismeflow/prisme-flow-docker:rpi

if [ $? -ne 0 ]; then
    echo "ERROR: Docker push failed for rpi image."
    exit 1
fi

docker push prismeflow/prisme-flow-docker:$PRISME_FLOW_VERSION-rpi

if [ $? -ne 0 ]; then
    echo "ERROR: Docker push failed for rpi image."
    exit 1
fi
