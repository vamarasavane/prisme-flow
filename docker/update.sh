if [ $# -ne 1 ]; then
    echo $0: usage: update.sh PRISME_FLOW_VERSION
    exit 1
fi

PRISME_FLOW_VERSION=$1

sed -i "" -e "s/\(version\": \"\).*\(\"\)/\1$PRISME_FLOW_VERSION\"/g" package.json
sed -i "" -e "s/\(node-red\": \"\).*\(\"\)/\1$PRISME_FLOW_VERSION\"/g" package.json
sed -i "" -e "s/\(VERSION=\).*/\1$PRISME_FLOW_VERSION/g" .travis.yml
