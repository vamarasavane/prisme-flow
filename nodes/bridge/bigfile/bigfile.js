/**
 * Created by prisme.io on 09/06/2017.
 *  Big Nodes principles:
 *
 *  #1 can handle big data
 *  #2 send start/end messages
 *  #3 tell what they are doing
 */



module.exports = function(RED) {

    "use strict";

    var biglib = require('node-red-biglib');

    function BigFileInNode(config) {

        RED.nodes.createNode(this, config);

        // new instance of biglib for this configuration
        var bignode = new biglib({ config: config, node: this, status: 'filesize', generator: 'filename' });

        // biglib changes the configuration to add some properties
        config = bignode.config();

        this.on('input', function(msg) {

          // if no configuration available from the incoming message, a new one is returned, cloned from default
          msg.config = bignode.new_config(msg.config);

          if (!config.nopayload) msg.filename = msg.filename || msg.payload;
          bignode.set_generator_property(msg);

          if (!msg.config.filename) throw new Error("No filename given");

          if (msg.config.flow == "blocks") {

            delete msg.config.format;
            return bignode.stream_file_blocks(msg);
          }

          if (msg.config.flow == "buffer") {

            delete msg.config.format;
            return bignode.stream_full_file(msg);
          }

          if (msg.config.flow == "lines") {

            return bignode.stream_data_lines(msg);
          }

          throw new Error("Unknown flow format: " + config.flow);

        });
    }

    RED.nodes.registerType("bigfile reader",BigFileInNode);
}
