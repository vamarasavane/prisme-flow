/**
 * Created by prisme.io on 09/06/2017.
 */

var ChatLog = require('./lib/chat-log.js')

module.exports = function (RED) {
  function BotLog (config) {
    RED.nodes.createNode(this, config)
    var node = this

    this.on('input', function (msg) {
      var chatContext = msg.chat()

      if (chatContext != null) {
        var chatLog = new ChatLog(chatContext)
        node.send(chatLog.message(msg))
      }
    })
  }
  RED.nodes.registerType('bot-log', BotLog)
}
