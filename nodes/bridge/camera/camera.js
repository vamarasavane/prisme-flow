/**
 * Created by prisme.io on 09/06/2017.
 */


var bodyParser = require('body-parser')

module.exports = function (RED) {

  function Node (config) {
    RED.nodes.createNode(this, config);
    var node = this;
    var requestSize = '50mb';


    RED.httpAdmin.post('/node-red-camera/:id', bodyParser.raw({ type: '*/*', limit: requestSize }), function(req,res) {

        var node = RED.nodes.getNode(req.params.id)

        if (node != null) {
            try {
                node.receive({payload: req.body})
                res.sendStatus(200)
            } catch(err) {
                res.sendStatus(500)
                node.error(RED._("upload-camera.failed", { error: err.toString() }))
            }
        } else {
            res.status(404).send("no node found")
        }
    })

    this.on('input', function (msg) {
      if(msg.payload !== '') {
        node.send(msg)
      }
    })
  }
  RED.nodes.registerType('camera', Node)
};
