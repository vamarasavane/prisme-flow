/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
/**
 * The CouchDBNode provides the implementation for retrieving data from the CouchDB
 * database.
 */
  function CouchDBNode (config) {
    var thisNode = this
    var nano = require('nano')(config.serverUrl)
    var db = nano.use(config.database)

    this.on('input', function (msg) {
      //
      // Retrieve byId
      //
      if (config.retrievalType == 'byId') {
        db.get(msg.payload, function (err, body) {
          if (!err) {
            msg.payload = body
            thisNode.send(msg)
          } // End of no error
        }) // End of db.get
      } // End of byId
      else
        //
        // Retrieve byView
        //
      if (config.retrievalType == 'byView') {
        // thisNode.log("key = " + JSON.stringify(msg.payload));
        var params = {
          include_docs: true
        }
        // If there is a msg.payload, use that as the search key.  Otherwise
        // no search key and all documents are returned.
        if (msg.payload) {
          params.key = msg.payload
        }
        db.view(config.designDoc, config.viewName, params, function (err, body) {
          if (!err) {
            // thisNode.log("Result from lookup: " + JSON.stringify(body));
            msg.payload = body.rows
            thisNode.send(msg)
          } // End of no error
        }) // End of db.view
      } // End of byView
    }) // End of on "input"
    RED.nodes.createNode(thisNode, config)
  } // End of CouchDBNode definition

/**
 * Insert data into the database
 * * serverUrl - The URL to reach the CouchDB server ... eg. http://localhost:5984
 * * database - The name of the database
 */
  function CouchDBInsertNode (config) {
    var thisNode = this
    var nano = require('nano')(config.serverUrl)
    var db = nano.use(config.database)
    this.on('input', function (msg) {
      // Process the insertion request here
      db.insert(msg.payload, function (err, body) {
        // Nothing to do on the callback
        if (err) {
          thisNode.warn('[' + config.type + ':' + config.name + ']: Error: ' + JSON.stringify(err))
        }
      }) // End of db.insert
    }) // End of on "input"
  } // End of CouchDBInsertNode

  RED.nodes.registerType('couchdb', CouchDBNode)
  RED.nodes.registerType('couchdbinsert', CouchDBInsertNode)
} // End of module.exports
// End of file
