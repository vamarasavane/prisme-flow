/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
  function RemoteServerNode (n) {
    RED.nodes.createNode(this, n)
    this.host = n.host
    this.port = n.port
  }
  RED.nodes.registerType('remote-server', RemoteServerNode)
}
