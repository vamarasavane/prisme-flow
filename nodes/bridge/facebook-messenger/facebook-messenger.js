/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
  const request = require('request')

  function checkmsg (msg) {
    if (!msg.payload) {
      return 'Missing property: msg.payload'
    }
    if (typeof (msg.payload) !== 'string') {
      return 'submitted msg.payload is not text'
    }
    if (!msg.facebookevent) {
      return 'Missing property: msg.facebookevent'
    }
    return ''
  }

  function checkevent (event) {
    if (!event.sender || !event.sender.id) {
      return 'Missing property: msg.facebookevent.sender.id'
    }
    // if (!event.recipient || !event.recipient.id) {
    //  return 'Missing property: msg.facebookevent.recipient.id';
    // }
    return ''
  }

  function sendReplyMessage (node, token, to, text) {
    var messageData = {
      recipient: {id: to},
      message: {text: text}
    }

    request({
      uri: 'https://graph.facebook.com/v2.6/me/messages',
      qs: {
        access_token: token},
      method: 'POST',
      json: messageData

    }, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var recipientId = body.recipient_id
        var messageId = body.message_id
      } else {
        node.status({fill: 'red', shape: 'dot', text: 'Error Sending Message'})
      }
    })
  }

  function Node (config) {
    var node = this
    RED.nodes.createNode(this, config)

    this.on('input', function (msg) {
      var message = ''
      var event = {}
      var token = this.credentials.token
      // var token = 'aaaa';
      if (!token) {
        message = 'Access Token must be specified in Node Configuration'
      }
      if (!message) {
        message = checkmsg(msg)
      }
      if (!message) {
        event = msg.facebookevent
        message = checkevent(event)
      }
      if (message) {
        node.status({fill: 'red', shape: 'dot', text: message})
        node.error(message, msg)
        return
      }
      var senderID = event.sender.id

      node.status({fill: 'blue', shape: 'dot', text: 'about to do something'})
      sendReplyMessage(this, token, senderID, msg.payload)
      node.status({fill: 'blue', shape: 'dot', text: 'did it'})

      node.status({})
      node.send(msg)
    })
  }

  RED.nodes.registerType('facebook-messenger-writer', Node, {
    credentials: {
      token: {type: 'text'}
    }
  })
}
