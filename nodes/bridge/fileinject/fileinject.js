/**
 * Created by prisme.io on 09/06/2017.
 */

var bodyParser = require('body-parser')

module.exports = function (RED) {
  var requestSize = '50mb'

  function Node (config) {
    RED.nodes.createNode(this, config);
    var node = this;

    RED.httpAdmin.post('/node-red-fileinject/:id', bodyParser.raw({ type: '*/*', limit: requestSize }), function(req,res) {

        var node = RED.nodes.getNode(req.params.id)

        if (node != null) {
            try {
                node.receive({payload: req.body})
                res.sendStatus(200)
            } catch(err) {
                res.sendStatus(500)
                node.error(RED._("inject-file.failed", { error: err.toString() }))
            }
        } else {
            res.status(404).send("no node found")
        }
    })

    this.on('input', function (msg) {
      if(msg.payload !== '') {
        node.send(msg)
      }
    })
  }
  RED.nodes.registerType('fileinject', Node)
};
