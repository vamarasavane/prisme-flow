/**
 * Created by prisme.io on 09/06/2017.
 */

var _ = require('lodash-node')
var moment = require('moment')

exports.getTime = function () {
  return moment().format('HH:mm')
}
