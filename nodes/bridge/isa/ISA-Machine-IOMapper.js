/**
 * Created by prisme.io on 09/06/2017.
 */

/**
 * That nodes is to communicate with the node-red-contrib-modbusio node package nodes.
 * https://www.npmjs.com/package/node-red-contrib-modbusio
 *
 * @namespace ISAMachineMapperIO
 * @param RED
 */

module.exports = function (RED) {
  'use strict'
  var opcua = require('node-opcua')
  var isaBasics = require('./core/isabasics')
  var isaOpcUa = require('./opcua/isaopcua')

  isaOpcUa.opcua = opcua

  function ISAMachineIOMapperNode (configNode) {
    RED.nodes.createNode(this, configNode)

    this.name = configNode.name
    this.topic = configNode.topic
    this.register = configNode.register
    this.group = configNode.group
    this.order = configNode.order
    this.mappings = configNode.mappings

    var node = this

    var machineConfig = RED.nodes.getNode(configNode.machineid)

    function verbose_warn (logMessage) {
      if (RED.settings.verbose) {
        node.warn((node.name) ? node.name + ': ' + logMessage : 'Machine-Mapper: ' + logMessage)
      }
    }

    function verbose_log (logMessage) {
      if (RED.settings.verbose) {
        node.log(logMessage)
      }
    }

    verbose_log('machine io mapper initialization ' + machineConfig.machine)

    this.on('input', function (msg) {
      var data = msg.payload

      if (!Array.isArray(msg.payload) ||
                node.register != msg.payload.length) {
        node.error("configured size doesn't match input length of register (array)")
        node.send(msg)
        return
      }

      var structuredValues = []

      if (node.mappings.length && node.mappings.length > 0 && data.length && data.length > 0) {
        verbose_log('node.mappings.length: ' + node.mappings.length + ' data.length:' + data.length)
        console.time('mappingio')

        node.mappings.forEach(function (mapping) {
          if (!mapping.structureNodeId) {
            verbose_warn('mapping.structureNodeId not valid ' + mapping.structureNodeId)
            return
          }

          var bitValue = 0
          var machineValue = 0

          if (mapping.quantity > 1) {
            bitValue = isaBasics.get_bits_from_quantity(mapping.quantity)
            if (bitValue) {
              machineValue = isaBasics.reconnect_values(bitValue, mapping.start, data)
            }
          } else {
            if (data.length >= mapping.quantity) {
              machineValue += data[mapping.start]
              bitValue = 16
            } else {
              node.error('check start and quantity of structureNodeId mapping ' + mapping.structureNodeId)
              return
            }
          }

          structuredValues.add(isaOpcUa.mapOpcUaMachineValue(mapping, machineValue, bitValue))
        })

        console.timeEnd('mappingio')
      } else {
        verbose_warn('there is no mapping configuration')
      }

      var sendMapping = isaOpcUa.newOpcUaMachineMapping(machineConfig, node)
      var sendWriteValue = isaOpcUa.writeOpcUaMachineMapping(machineConfig, node, structuredValues)

      msg = [{payload: data}, {payload: sendWriteValue}, {payload: sendMapping}]

      node.send(msg)
    })

    this.on('close', function () {
      verbose_log('machine io mapper close')
      machineConfig = null
    })
  }

  RED.nodes.registerType('ISA-Machine-IOMapper', ISAMachineIOMapperNode)
}
