/**
 * Created by prisme.io on 09/06/2017.
 */

/**
 * That nodes is to communicate with the node-red-contrib-modbus node package nodes.
 * see https://www.npmjs.com/package/node-red-contrib-modbus
 *
 * @namespace ISAMachineMapper
 * @param RED
 */
module.exports = function (RED) {
  'use strict'

  var opcua = require('node-opcua')
  var isaBasics = require('./core/isabasics')
  var isaOpcUa = require('./opcua/isaopcua')

  isaOpcUa.opcua = opcua

  function ISAMachineMapperNode (configNode) {
    RED.nodes.createNode(this, configNode)

    this.name = configNode.name
    this.topic = configNode.topic
    this.register = configNode.register
    this.group = configNode.group
    this.order = configNode.order
    this.mappings = configNode.mappings

    var node = this

    var machineConfig = RED.nodes.getNode(configNode.machineid)

    function verbose_warn (logMessage) {
      if (RED.settings.verbose) {
        node.warn((node.name) ? node.name + ': ' + logMessage : 'Machine-Mapper: ' + logMessage)
      }
    }

    function verbose_log (logMessage) {
      if (RED.settings.verbose) {
        node.log(logMessage)
      }
    }

    verbose_log('machine mapper initialization ' + machineConfig.machine)

        /**
         * The mapper node needs an array of 16Bit chunks (as read from Modbus) or values.
         * The length of the array has to be the same as configured in the node config, otherwise it stops
         * writing and mapping the incoming data.
         *
         *
         */
    this.on('input', function (msg) {
      var data = msg.payload

      if (!Array.isArray(msg.payload) ||
                node.register != msg.payload.length) {
        node.error("configured size doesn't match input length of register (array)")
        node.send(msg)
        return
      }

      verbose_log('machineConfig: ' + machineConfig.machine + ' on interface ' + machineConfig.interface)

      var structuredValues = {}

      if (node.mappings.length && node.mappings.length > 0 && data.length && data.length > 0) {
        verbose_log('node.mappings.length: ' + node.mappings.length + ' data.length:' + data.length)
        console.time('mapping')

        node.mappings.forEach(function (mapping) {
          if (!mapping.structureNodeId) {
            verbose_warn('mapping.structureNodeId not valid ' + mapping.structureNodeId)
            return
          }

          var bitValue = 0
          var machineValue = 0

          if (mapping.quantity > 1) {
            bitValue = isaBasics.get_bits_from_quantity(mapping.quantity)
            if (bitValue) {
              machineValue = isaBasics.reconnect_values(bitValue, mapping.start, data)
            }
          } else {
            if (data.length >= mapping.quantity) {
              machineValue += data[mapping.start]
              bitValue = 16
            } else {
              node.error('check start and quantity of structureNodeId mapping ' + mapping.structureNodeId)
              return
            }
          }

          structuredValues[mapping.structureNodeId] = isaOpcUa.mapOpcUaMachineValue(mapping, machineValue, bitValue)
        })

        console.timeEnd('mapping')
      }

      var sendMapping = isaOpcUa.newOpcUaMachineMapping(machineConfig, node, structuredValues)
      var sendWriteValue = isaOpcUa.writeOpcUaMachineMapping(machineConfig, node, structuredValues)

      msg = [{payload: data}, {payload: sendWriteValue}, {payload: sendMapping}]

      node.send(msg)
    })

    this.on('close', function () {
      verbose_log('machine mapper close')
      machineConfig = null
    })
  }

  RED.nodes.registerType('ISA-Machine-Mapper', ISAMachineMapperNode)
}
