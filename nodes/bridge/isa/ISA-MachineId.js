/**
 * Created by prisme.io on 09/06/2017.
 */

/**
 * That node is to centralize the information about the machine.
 * With this node you have to configure the machine information only once
 * and you can use it in many mapping nodes.
 *
 * @namespace ISAMachine
 * @param RED
 */
module.exports = function (RED) {
  function ISAMachineIdNode (configNode) {
    RED.nodes.createNode(this, configNode)

    this.machine = configNode.machine
    this.protocolName = configNode.protocolName

    var node = this

    var machineInfo = 'machine ' + node.machine + ' on interface ' + node.protocolName

    function verbose_warn (logMessage) {
      if (RED.settings.verbose) {
        node.warn('MachineId -> ' + logMessage + ' ' + machineInfo)
      }
    }

    function verbose_log (logMessage) {
      if (RED.settings.verbose) {
        node.log('MachineId -> ' + logMessage + ' ' + machineInfo)
      }
    }

    verbose_log('initialization')

    node.on('close', function () {
      verbose_log('close')
    })
  }

  RED.nodes.registerType('ISA-MachineId', ISAMachineIdNode)
}
