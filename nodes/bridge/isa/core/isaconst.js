/**
 * Created by prisme.io on 09/06/2017.
 */

'use strict'

/**
 * Basic constant messages.
 * @module ISAConstants
 */
var ISAResultMessage = {}

module.exports = {
    /**
     * Constants of results in ISA context.
     * @constant
     * @property {number} ISAResultMessage.None                             - None
     * @property {number} ISAResultMessage.ReferenceNotFound                - The reference wasn't found
     * @property {number} ISAResultMessage.NodeIdNotValid                   - The NodeId isn't valid
     * @property {number} ISAResultMessage.AddressSpaceNotValid             - The address space isn't valid
     * @property {number} ISAResultMessage.AddressSpaceOperationDone        - The address space operation is done
     * @property {number} ISAResultMessage.ParentNodeReferenceNotFound      - The parent NodeId was not found
     * @property {number} ISAResultMessage.ServerNotReady                   - The server is not ready for address space operations
     */
  ISAResultMessage: ISAResultMessage
}

Object.defineProperty(ISAResultMessage, 'None', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 0
})

Object.defineProperty(ISAResultMessage, 'ReferenceNotFound', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 1
})

Object.defineProperty(ISAResultMessage, 'NodeIdNotValid', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 2
})

Object.defineProperty(ISAResultMessage, 'AddressSpaceNotValid', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 3
})

Object.defineProperty(ISAResultMessage, 'AddressSpaceOperationDone', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 4
})

Object.defineProperty(ISAResultMessage, 'ParentNodeReferenceNotFound', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 5
})

Object.defineProperty(ISAResultMessage, 'ServerNotReady', {
  writable: false,
  enumerable: true,
  configurable: false,
  value: 6
})
