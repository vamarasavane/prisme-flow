/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
  function ISA95BatchMLHelperNode (config) {
    RED.nodes.createNode(this, config)
    this.action = config.action

    var node = this

    function verbose_warn (logMessage) {
      if (RED.settings.verbose) {
        node.warn((node.name) ? node.name + ': ' + logMessage : 'BatchML-Helper: ' + logMessage)
      }
    }

    function verbose_log (logMessage) {
      if (RED.settings.verbose) {
        node.log(logMessage)
      }
    }

    node.on('input', function (msg) {
      verbose_log('ISA95BatchMLHelperNodeAction: ' + node.action)

      node.send(msg)
    })

    node.on('close', function () {
      verbose_log('close')
    })
  }

  RED.nodes.registerType('BatchML-Helper', ISA95BatchMLHelperNode)
}
