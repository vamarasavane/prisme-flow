/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
  function ISA95B2MMLHelperNode (config) {
    RED.nodes.createNode(this, config)
    this.action = config.action

    var node = this

    function verbose_warn (logMessage) {
      if (RED.settings.verbose) {
        node.warn((node.name) ? node.name + ': ' + logMessage : 'B2MML-Helper: ' + logMessage)
      }
    }

    function verbose_log (logMessage) {
      if (RED.settings.verbose) {
        node.log(logMessage)
      }
    }

    node.on('input', function (msg) {
      verbose_log('ISA95B2MMLHelperNodeAction: ' + node.action)

      node.send(msg)
    })

    node.on('close', function () {
      verbose_log('close')
    })
  }

  RED.nodes.registerType('B2MML-Helper', ISA95B2MMLHelperNode)
}
