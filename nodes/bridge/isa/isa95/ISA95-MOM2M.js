/**
 * Created by prisme.io on 09/06/2017.
 */

module.exports = function (RED) {
  'use strict'
  var isaBasics = require('./../core/isabasics')

  function ISA95MOM2MNode (config) {
    RED.nodes.createNode(this, config)

    this.name = config.name
    this.register = config.register
    this.mappings = config.mappings

    var node = this

    var machineConfig = RED.nodes.getNode(config.machineid)

    this.on('input', function (msg) {
      var data = msg.payload

      if (msg.payload.length != node.register) {
        node.send(msg)
        return
      }

      var bitValue = 0
      var namedValues = []

      node.mappings.forEach(function (mapping) {
        bitValue = isaBasics.get_bits_from_quantity(mapping.quantity)
        var machineValue = isaBasics.reconnect_values(bitValue, mapping.start, msg.payload)
        namedValues.add(isaBasics.mapMachineValue(mapping, machineValue, bitValue))
      })

      var sendMapping = isaBasics.newMachineMapping(machineConfig, node)
      var sendWriteValue = isaBasics.writeMachineMapping(machineConfig, node, namedValues)

      msg = [{payload: data}, {payload: sendWriteValue}, {payload: sendMapping}]

      node.send(msg)
    })
  }

  RED.nodes.registerType('ISA95-MOM2M', ISA95MOM2MNode)
}
