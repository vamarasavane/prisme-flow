/**
 * Created by prisme.io on 09/06/2017.
 */

'use strict'

var isaOpcUa
var dynamicNodes
/**
 * ISA functions to work with mappings.
 * @module ISAMapping
 */
module.exports = {

  isaOpcUa: isaOpcUa,
  dynamicNodes: dynamicNodes,

    /**
     * Searching the containing of the property "mappingType".
     * @function
     * @param {object} payload - payload of msg object
     * @returns {boolean} true if found, otherwise false
     */
  contains_mappingType: function (payload) {
    return payload.hasOwnProperty('mappingType')
  },
    /**
     * Searching the containing of the property "messageType".
     * @function
     * @param {object} payload - payload of msg object
     * @returns {boolean} true if found, otherwise false
     */
  contains_messageType: function (payload) {
    return payload.hasOwnProperty('messageType')
  },
    /**
     * Searching the containing of the property "opcuaCommand".
     * @function
     * @param {object} payload - payload of msg object
     * @returns {boolean} true if found, otherwise false
     */
  contains_opcua_command: function (payload) {
    return payload.hasOwnProperty('opcuaCommand')
  },
    /**
     * Read mapped objects from a dynamic list of address space objects.
     * @function
     * @param {String} nodeId - nodeId to search for
     * @param {object} variableDatatype - OPC UA variable datatype
     * @returns {object} item of list
     */
  search_mapped_to_read: function (parentNodeId, nodeId) {
    if (!nodeId) {
      throw new Error('search_mapped_to_read nodeId have to be a valid object')
    }

    if (module.exports.dynamicNodes.hasOwnProperty(nodeId.toString())) {
            // console.log(nodeId.toString() + " -> " + module.exports.dynamicNodes[nodeId.toString()]);
      return module.exports.dynamicNodes[nodeId.toString()]
    } else {
            // console.log(nodeId.toString() + " is not in dynamic node set");
    }
  },
    /**
     * Add mapped object of address space objects to dynamic list.
     * @function
     * @param parentNodeId - node id of parent
     * @param {String} nodeId - nodeId to search for
     * @param {object} variableDatatype - OPC UA variable datatype
     * @returns {*} item of list
     */
  add_mapped_to_list: function (parentNodeId, nodeId, variableDatatype) {
    if (!nodeId) {
      throw new Error('add_mapped_to_list nodeId have to be a valid object')
    }

    if (module.exports.dynamicNodes.hasOwnProperty(nodeId.toString())) {
      return module.exports.dynamicNodes[nodeId.toString()]
    }

    var item = {
      'parentNodeId': parentNodeId,
      'nodeId': nodeId,
      'value': undefined,
      'writtenValue': undefined,
      'variableDatatype': variableDatatype
    }

        // console.log("adding " + nodeId.toString() + " to dynamic node set");
    module.exports.dynamicNodes[nodeId.toString()] = item

    return item
  },
    /**
     * Write mapped or adds mapping object to a dynamic list of address space objects.
     * @function
     * @param parentNodeId - node id of parent
     * @param {String} nodeId - nodeId to search for
     * @param {object} value - value to write
     * @param {object} typeStructure - to find the init value @see ISAOPCUA:mapOpcUaDatatypeAndInitValue
     * @returns {*} item if found, otherwise item created
     */
  search_mapped_to_write: function (parentNodeId, nodeId, value, typeStructure) {
    if (!nodeId) {
      throw new Error('search_mapped_to_write nodeId have to be a valid object')
    }

    var item

    if (module.exports.dynamicNodes.hasOwnProperty(nodeId.toString())) {
      item = module.exports.dynamicNodes[nodeId.toString()]
      item = this.set_item_value(item, value)
    } else {
      var itemDefaultSettings = module.exports.isaOpcUa.mapOpcUaDatatypeAndInitValue(typeStructure)

      if (itemDefaultSettings) {
        item = {
          'parentNodeId': parentNodeId,
          'nodeId': nodeId,
          'value': value,
          'writtenValue': value,
          'variableDatatype': itemDefaultSettings.variableDatatype
        }

        this.set_item_value(item, value)
        module.exports.dynamicNodes[nodeId.toString()] = item
      }
    }

    return item
  },
    /**
     * Write value to item with special handling for boolean
     * @function
     * @param {object} item - item to write property value
     * @param {object} value - value to write
     * @returns {object} item with value
     */
  set_item_value: function (item, value) {
    if (item.variableDatatype === 'Boolean') {
      if (value) {
        item.value = true
      } else {
        item.value = false
      }
    } else {
      item.value = value
    }

    return item
  }
}
