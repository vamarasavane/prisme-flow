/**
 * Created by prisme.io on 09/06/2017.
 */


module.exports = function (RED) {
    "use strict";

    function OpcUaEndpointNode(n) {

        RED.nodes.createNode(this, n);

        this.endpoint = n.endpoint;
	this.securityPolicy = n.secpol;
	this.securityMode = n.secmode;
        this.login = n.login;

        if (this.credentials) {
            this.user = this.credentials.user;
            this.password = this.credentials.password;
        }
    }

    RED.nodes.registerType("OpcUa-Endpoint", OpcUaEndpointNode, {

        credentials: {
            user: {type: "text"},
            password: {type: "password"}
        }
    });

};
