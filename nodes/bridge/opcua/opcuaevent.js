/**
 * Created by prisme.io on 09/06/2017.
 */


module.exports = function (RED) {
    "use strict";
    var opcua = require('node-opcua');
    var opcuaBasic = require('./opcua-basics');

    function OpcUaEventNode(n) {

        RED.nodes.createNode(this, n);

        this.root = n.root;         	// OPC UA item nodeID
        this.eventtype = n.eventtype; 	// eventType
        this.name = n.name;			    // Node name

        var node = this;

        node.on("input", function (msg) {

            // var baseEventTypeId = "i=2041";
            // var serverObjectId = "i=2253";
            // All event field, perhaps selectable in UI

            var basicEventFields = opcuaBasic.getBasicEventFields();
            var eventFilter = opcua.constructEventFilter(basicEventFields);

            msg.topic = node.root; // example: ns=0;i=85;
            msg.eventFilter = eventFilter;
            msg.eventFields = basicEventFields;
            msg.eventTypeIds = node.eventtype; // example: ns=0;i=10751;

            node.send(msg);
        });
    }

    RED.nodes.registerType("OpcUa-Event", OpcUaEventNode);
};
