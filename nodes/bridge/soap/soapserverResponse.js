/**
 * Created by prisme.io on 09/06/2017.
 */
module.exports = function(RED) {
  function SoapServerResponseNode(config) {
    var thisNode = this;

    RED.nodes.createNode(thisNode, config);

    // node-specific code goes here
    // Register the function to be called when an input event arrives that needs
    // to be processed.  Our logic is to retrieve the callback function used to
    // send back a soap response.  This should be carried in the msg.  If not
    // found, this results in a warning and the function ends.
    //
    // We build a response message for the data to be sent back that is contained in
    // the msg payload property.
    //
    thisNode.on("input", function(msg) {
      var callback = msg["_soapServer_soapResponseCallback"];
      if (callback == null) {
        thisNode.warn("No previous soap server found for a soap server response.");
        return;
      }
      var payload = msg.payload;
      callback({"payload": payload});
    });
  }
  RED.nodes.registerType("soapserverResponse", SoapServerResponseNode);
}
// End of file
